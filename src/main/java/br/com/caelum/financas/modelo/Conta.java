package br.com.caelum.financas.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class Conta {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private String titular;
    private String banco;
    private String numero;
    private String agencia;

    public Conta(Long id, String titular, String banco, String numero, String agencia) {
        this.id = id;
        this.titular = titular;
        this.banco = banco;
        this.numero = numero;
        this.agencia = agencia;
    }

    public Conta() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    @Override
    public String toString() {
        return "Titular: " + titular + " - "+
                "Banco: " + banco + " - " +
                "Numero: " + numero + " - " +
                "Agencia: " + agencia;
    }
}
